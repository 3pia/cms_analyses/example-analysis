# VH

## Setup

- `source setup.sh`
- `law index`

## Tasks
### Previous Tasks (usally already executed)
- DownloadFiles
- DownloadNanoAODs
- Corrections

### Tasks to execute
The task chain runs as follows:
- CoffeaProcessor Processor 
- GroupCoffeaProcessor
- Rebinning
- Plotting/Datacard

The DNN-training and exporting of variables to arrays is a different step:
 - CoffeaProcessor - ExporterProcessor
 - DNN-training tasks
 - insert model-path

***Law-tips***
  Run all the following commands inside the `common` directory.
  When you execute a law task by `law run TASKNAME --param1 --param2 ...` you can add one of the following to 
  check some properties of the task-chain.
  If you let for example the `PlotProducer` run and the `CoffeaProcessor` has not run yet, it will run automatically.
  Thus, you do not need to run all the commands seperately.
  
  The `--print-status X` flag with `X` being an integer (0, 1, 2, ... or -1), shows you the status of tasks.

  The `--print-output X` flag with `X` being an integer (0, 1, 2, ... or -1), shows you the output of tasks.

  The `--remove-output X` flag with `X` being an integer (0, 1, 2, ... or -1),  *DELETES* the output of tasks! This
  should be used very carefully.

***Quick testing runs ***
  - `debug` only: run locally on debug dataset (defined in Processor) only
  - `explorative` only: run on one file from each dataset using Dask

**1. CoffeaProcessor (Histogramer or Exporter)**:

  ```shell
  law run CoffeaProcessor --version test_01 --recipe selection --processor Processor --analysis-choice vh --year 2017 --debug
  ```

**2. GroupCoffeaProcesses (after Histogramer)**:
**3. Rebin**:
**4. Plotting or DatacardProducer**:
  ```shell
  law run PlotProducer --version test_01 --recipe selection --analysis-choice vh --log-scale --process-group plotting --model vh-model
  ```
  ```shell
  law run DatacardProducer --version test_01 --recipe selection --local-scheduler --explorative
  ```
## Multiclassification

First run data-preparation on free machein (vispa-gpu10 for example)

```shell
for SPLIT in 0 1 2 3 4; do law run vh.MulticlassDataprep --version dev1 --local-scheduler --split $SPLIT; done
```

```shell
law run vh.MulticlassStitched --version dev1 --local-scheduler --vh.MulticlassTraining-comet --vh.MulticlassTraining-htcondor-logs --workers 2
```

The `workers 2` defines how many htcondor-tasks are submitted.


## Fit - Combine

### Combine

[https://github.com/cms-analysis/CombineHarvester](https://github.com/cms-analysis/CombineHarvester "CombineHarvester")


Combine works with a workspace that is created from your datacard

Text $\rightarrow$ workspace

```shell
text2workspace.py </path/to/datacard.txt> -o workspace.root -m 125
```

Because the tool is old, you have to set the parameter for the higgs mass 125 (`-m 125`). This is not actually used but rather just a technical thing.



#### Upper limit

```shell
combine -M AsymptoticLimits workspace.root --verbose 1 --mass 125 --run expected --noFitAsimov -t -1
```

#### Multidim-fit

```shell
combine -M MultiDimFit workspace.root --verbose 1 --mass 125 -t -1 --robustFit 1  --setParameters r=1 --freezeParameters r
```


#### Datacard

Processes:
- Signal process: index $\leq$ 0
- Background process: index $>$ 0

Physics model:
- expr( proc A / proc B, scaling signal strength )
