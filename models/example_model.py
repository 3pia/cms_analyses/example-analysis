# coding: utf-8

import os
import numpy as np
import hist
import example_analysis.config.analysis as example_config
from utils.datacard import Datacard, RDict

# from utils.util import reluncs, wquant
# from enum import IntEnum, unique
# from rich.console import Console
import rich.table


class StatModel(Datacard):
    @property
    def custom_lines(self):
        return [
            f"{self.bin} autoMCStats 10",
        ]

    @classmethod
    def requires(cls, task):
        from tasks.group import Rebin

        return Rebin.req(
            task, process_group=("default",)
        )  # "plotting")  # list(cls.processes))  # + ["data"]
