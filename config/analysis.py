# coding: utf-8
# flake8: noqa

"""
Definition of an analysis example
"""

import scinum as sn
import utils.aci as aci

#
# analysis and config
#
# import config.Run2_pp_13TeV_2017 as run_2017
from config.analysis import analysis
from config.util import PrepareConfig

# get = od.Process.get_instance


def rgb(r, g, b):
    return (r, g, b)


analysis = analysis.copy(name="vh")

get = analysis.processes.get

# corrections which are only used for distinct analyses
analysis.aux["non_common_corrections"] = []
analysis.aux["signal_process"] = "dy"
analysis.aux["stat_model"] = "example_model"

# add analysis specific processes
specific_processes = []

#
# process groups
#
analysis.aux["process_groups"] = {
    "default": [
        "dy",
        "tt",
        "ttV",
        "ttVH",
        "ttVV",
    ],
    "plotting": [
        "dy",
        "tt",
        "ttV",
        "ttVH",
        "ttVV",
    ],
}

analysis.aux["btag_sf_shifts"] = [
    "lf",
    "lfstats1",
    "lfstats2",
    "hf",
    "hfstats1",
    "hfstats2",
    "cferr1",
    "cferr2",
]

PrepareConfig(
    analysis,
    processes=[
        "dy_lep_10To50",
        "dy_lep_nj",
        "tt",
        "ttV",
        "ttVH",
        "ttVV",
    ],
    ignore_datasets=[],
    allowed_exclusive_processes=[],
)

from example_analysis.config.categories import setup_categories

setup_categories(analysis)

from example_analysis.config.variables import setup_variables

setup_variables(analysis)
