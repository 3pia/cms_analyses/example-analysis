# coding: utf-8
# flake8: noqa
# from .constants import Z_MASS
from itertools import chain
import utils.aci as aci

Z_MASS = 91.1876

variables_dict = {
    "MET": {
        "expression": "met.pt",
        "binning": (25, 0.0, 500.0),
        "unit": "GeV",
        "x_title": r"$p_{T}^{miss}$",
        "aux": {"plot": True},
    },
    "mll": {
        "expression": "mll",
        "binning": (20, Z_MASS - 20.0, Z_MASS + 20.0),
        "unit": "GeV",
        "x_title": r"$m_{ll}$",
        "aux": {"plot": True},
    },
    "njets": {
        "expression": "njets",
        "binning": (10, 0.0, 10.0),
        "x_title": r"Number of jets",
        "aux": {"plot": True},
    },
    **dict(
        list(
            chain.from_iterable(
                [
                    [
                        (
                            "jet{}_pt".format(i),
                            {
                                "expression": "util.normalize(good_jets.pt, pad=4, clip=True)[:, {}]".format(
                                    i - 1
                                ),
                                "binning": (25, 0.0, 500.0),
                                "unit": "GeV",
                                "x_title": r"Jet %s $p_{T}$" % i,
                                "aux": {"plot": True},
                            },
                        ),
                        (
                            "jet{}_eta".format(i),
                            {
                                "expression": "util.normalize(good_jets.eta, pad=4, clip=True)[:, {}]".format(
                                    i - 1
                                ),
                                "binning": (25, -2.5, 2.5),
                                "x_title": r"Jet %s $\eta$" % i,
                                "aux": {"plot": True},
                            },
                        ),
                        (
                            "jet{}_phi".format(i),
                            {
                                "expression": "util.normalize(good_jets.phi, pad=4, clip=True)[:, {}]".format(
                                    i - 1
                                ),
                                "binning": (25, -3.14, 3.14),
                                "x_title": r"Jet %s $\phi$" % i,
                                "aux": {"plot": True},
                            },
                        ),
                        (
                            "jet{}_btag".format(i),
                            {
                                "expression": "util.normalize(good_jets.btagDeepFlavB, pad=4, clip=True)[:, {}]".format(
                                    i - 1
                                ),
                                "binning": (10, 0.0, 1.0),
                                "x_title": r"Jet %s btag value" % i,
                                "aux": {"plot": True},
                            },
                        ),
                    ]
                    for i in range(1, 4 + 1)
                ]
            )
        )
    ),
    # pt, eta, phi of jets
    **dict(
        list(
            chain.from_iterable(
                [
                    [
                        (
                            "electron{}_pt".format(i),
                            {
                                "expression": "util.normalize(good_electrons.pt, pad=2, clip=True)[:, {}]".format(
                                    i - 1
                                ),
                                "binning": (25, 0.0, 500.0),
                                "unit": "GeV",
                                "x_title": r"Electron %s $p_{T}$" % i,
                                "aux": {"plot": True},
                            },
                        ),
                        (
                            "electron{}_eta".format(i),
                            {
                                "expression": "util.normalize(good_electrons.eta, pad=2, clip=True)[:, {}]".format(
                                    i - 1
                                ),
                                "binning": (25, -2.5, 2.5),
                                "x_title": r"Electron %s $\eta$" % i,
                                "aux": {"plot": True},
                            },
                        ),
                        (
                            "electron{}_phi".format(i),
                            {
                                "expression": "util.normalize(good_electrons.phi, pad=2, clip=True)[:, {}]".format(
                                    i - 1
                                ),
                                "binning": (25, -3.14, 3.14),
                                "x_title": r"Electron %s $\phi$" % i,
                                "aux": {"plot": True},
                            },
                        ),
                        (
                            "muon{}_pt".format(i),
                            {
                                "expression": "util.normalize(good_muons.pt, pad=2, clip=True)[:, {}]".format(
                                    i - 1
                                ),
                                "binning": (25, 0.0, 500.0),
                                "unit": "GeV",
                                "x_title": r"Muon %s $p_{T}$" % i,
                                "aux": {"plot": True},
                            },
                        ),
                        (
                            "muon{}_eta".format(i),
                            {
                                "expression": "util.normalize(good_muons.eta, pad=2, clip=True)[:, {}]".format(
                                    i - 1
                                ),
                                "binning": (25, -2.5, 2.5),
                                "x_title": r"Muon %s $\eta$" % i,
                                "aux": {"plot": True},
                            },
                        ),
                        (
                            "muon{}_phi".format(i),
                            {
                                "expression": "util.normalize(good_muons.phi, pad=2, clip=True)[:, {}]".format(
                                    i - 1
                                ),
                                "binning": (25, -3.14, 3.14),
                                "x_title": r"Muon %s $\phi$" % i,
                                "aux": {"plot": True},
                            },
                        ),
                    ]
                    for i in range(1, 2 + 1)
                ]
            )
        )
    ),
}


def setup_variables(cfg):
    from processor.util import normalize

    for name, attributes in variables_dict.items():
        cfg.variables.add(
            aci.Variable(
                name=name,
                expression="normalize({})".format(attributes.pop("expression", None)),
                **attributes
            )
        )
